package waterpools;

public class DefaultLandscapeValidator implements LandscapeValidator {

    private final int maxHillsCount;
    private final int maxHillHeight;

    public DefaultLandscapeValidator(int maxHillsCount, int maxHillHeight) {
        this.maxHillsCount = maxHillsCount;
        this.maxHillHeight = maxHillHeight;
    }

    @Override
    public boolean landscapeIsValid(int[] landscape) {
        return landscape != null && landscape.length <= maxHillsCount;
    }

    @Override
    public boolean hillIsValid(int hillIdx, int[] landscape) {
        int hillHeight = landscape[hillIdx];
        return hillHeight >= 0 && hillHeight <= maxHillHeight;
    }
}
