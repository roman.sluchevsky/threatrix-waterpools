package waterpools;

import java.util.Stack;

public class WaterPoolCalculator {

    private final LandscapeValidator landscapeValidator;

    public WaterPoolCalculator(LandscapeValidator landscapeValidator) {
        this.landscapeValidator = landscapeValidator;
    }


    public long calculateWaterAmount(int[] landscape) {

        if (!landscapeValidator.landscapeIsValid(landscape)) {
            throw new IllegalArgumentException("Landscape validation failed");
        }

        long waterAmount = 0;
        Stack<Integer> leftBorderIndexes = new Stack<>();

        for (int currentHillIdx = 0; currentHillIdx < landscape.length; currentHillIdx++) {

            if(!landscapeValidator.hillIsValid(currentHillIdx, landscape)){
                throw new IllegalArgumentException("Hill validation failed");
            }

            if (isRightBorder(currentHillIdx, landscape)) {
                while (!leftBorderIndexes.isEmpty()) {
                    int leftBorderIdx = leftBorderIndexes.peek();
                    waterAmount += calculateWaterAmountAndRemovePoolFromLandscape(currentHillIdx, leftBorderIdx, landscape);

                    if (landscape[leftBorderIdx] <= landscape[currentHillIdx]) {
                        leftBorderIndexes.pop();
                    } else {
                        break;
                    }
                }
            }

            if (isLeftBorder(currentHillIdx, landscape)) {
                leftBorderIndexes.push(currentHillIdx);
            }
        }

        return waterAmount;
    }


    /**
     * Calculates and returns amount of water in a pool enclosed between {leftBorderIdx} and {rightBorderIdx}.
     * Water in the pool is replaced with hills after calculation is done
     */
    private long calculateWaterAmountAndRemovePoolFromLandscape(int rightBorderIdx, int leftBorderIdx, int[] landscape) {

        // valid pool must contain at least 1 hill between borders
        if(rightBorderIdx - leftBorderIdx < 2){
            return 0;
        }

         // Due to algorithm, pool bottom between borders is always flat (i.e. every hill between border hills have the same height).
        int poolBottomHeight = landscape[rightBorderIdx - 1];

        int poolMinBorderHeight = Math.min(landscape[leftBorderIdx], landscape[rightBorderIdx]);
        int poolHeight = poolMinBorderHeight - poolBottomHeight;
        int poolWidth = rightBorderIdx - leftBorderIdx - 1;
        for (int hillBetweenBordersIdx = leftBorderIdx + 1; hillBetweenBordersIdx < rightBorderIdx; hillBetweenBordersIdx++) {
            // do a trick to remove just calculated water from landscape so that it can't be calculated twice
            landscape[hillBetweenBordersIdx] = poolMinBorderHeight;
        }

        return (long) poolHeight * poolWidth;
    }


    /**
     * Returns true if hill at {currentPosition} is a left border of some water pool: i.e. it is higher than next hill
     */
    private boolean isLeftBorder(int currentPosition, int[] landscape) {
        return beforePenultimatePosition(currentPosition, landscape) &&
                landscape[currentPosition] > landscape[currentPosition + 1];
    }

    /**
     * Returns true if hill at {currentPosition} is a right border of some water pool: i.e. it is higher than previous hill
     */
    private boolean isRightBorder(int currentPosition, int[] landscape) {
        return currentPosition > 0 &&
                landscape[currentPosition] > landscape[currentPosition - 1];
    }

    private static boolean beforePenultimatePosition(int currentPosition, int[] landscape) {
        return currentPosition < landscape.length - 2;
    }

}
