package waterpools;

public class Main {

    public static void main(String args[]) {

        int[] landscape = new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1};
//        int[] landscape = new int[]{1, 2, 6, 3, 2, 1, 2, 4, 3, 8, 5, 0, 5, 5, 5, 4, 6, 3, 2};

        System.out.println(
                new WaterPoolCalculator(new DefaultLandscapeValidator(32_000, 32_000))
                        .calculateWaterAmount(landscape)
        );
    }

}
