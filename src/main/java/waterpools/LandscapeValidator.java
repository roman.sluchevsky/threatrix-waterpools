package waterpools;

public interface LandscapeValidator {

    boolean landscapeIsValid(int[] landscape);

    boolean hillIsValid(int hillHeight, int[] landscape);

}
