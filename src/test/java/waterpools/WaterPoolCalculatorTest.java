package waterpools;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WaterPoolCalculatorTest {

    private static final int MAX_HILLS_COUNT = 10;
    private static final int MAX_HILL_HEIGHT = 10;

    private final WaterPoolCalculator unitUnderTest = new WaterPoolCalculator(
            new DefaultLandscapeValidator(MAX_HILLS_COUNT, MAX_HILL_HEIGHT)
    );

    @Test
    public void testAgainstReferenceTestData(){
        int[] referenceTestData = new int[]{5,2,3,4,5,4,0,3,1};
        long referenceTestResult = 9;

        assertEquals(referenceTestResult, unitUnderTest.calculateWaterAmount(referenceTestData));
    }

    @Test
    public void when_landscape_empty__then_zero_amount(){
        assertEquals(0, unitUnderTest.calculateWaterAmount(new int[]{}));
    }

    @Test
    public void when_only_ascending_landscape__then_zero_amount(){
        assertEquals(0, unitUnderTest.calculateWaterAmount(new int[]{0, 1, 2, 3, 4, 5}));
    }

    @Test
    public void when_only_descending_landscape__then_zero_amount(){
        assertEquals(0, unitUnderTest.calculateWaterAmount(new int[]{5, 4, 3, 2, 1, 0}));
    }

    @Test
    public void when_only_flat_landscape__then_zero_amount(){
        assertEquals(0, unitUnderTest.calculateWaterAmount(new int[]{5, 5, 5, 5, 5}));
    }

    @Test
    public void when_hills_count_exceeded__than_throw(){
        assertThrows(IllegalArgumentException.class, () -> unitUnderTest.calculateWaterAmount(
                new Random().ints(MAX_HILLS_COUNT + 1, 0, MAX_HILL_HEIGHT).toArray()
        ));
    }

    @Test
    public void when_hills_height_exceeded__than_throw(){
        assertThrows(IllegalArgumentException.class, () -> unitUnderTest.calculateWaterAmount(
                new int[]{MAX_HILL_HEIGHT + 1}
        ));
    }

}
